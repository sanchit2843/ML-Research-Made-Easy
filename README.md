# Machine Learning papers to their blogs

<div align="left">
<h1>
    <img alt="HEADER" src="start.jpg" width="900" height="300"></img>
</h1>

This repository provides Machine Learning papers with their supplementary material links. Given the fast paced research in the field resorting to blogs for getting an idea of the paper and then deciding whether to invest the time on reading the paper looks like a wise approach.

### Caution : Don't understand this as a subtitute to reading papers but just for getting an idea of the paper and saving time by not going into the math directly.

This is an attempt to make  one stop for all types of machine learning papers and their easy to read blogs. If you feel you have some blogs that you want to be featured here, let me know.

This summary is categorized into:

- [Natural language Processing](https://github.com/purvanshi/ML-Research-Made-Easy#NLP)
  - [Question Answering](https://github.com/purvanshi/ML-Research-Made-Easys#Language-Modelling)
  - [Natural Language Inference](https://github.com/purvanshi/ML-Research-Made-Easy#supervised-learning) 
- [Computer vision](https://github.com/purvanshi/ML-Research-Made-Easy#computer-vision)
- [Speech](https://github.com/purvanshi/ML-Research-Made-Easy#speech)
- [Transfer Learning](https://github.com/purvanshi/ML-Research-Made-Easy#transfer-learning)
- [Reinforcement Learning](https://github.com/purvanshi/ML-Research-Made-Easy#reinforcement-learning)
- [Unsupervised Learning](https://github.com/purvanshi/ML-Research-Made-Easy#unsupervised-learning)
- [Semi supervised Learning](https://github.com/purvanshi/ML-Research-Made-Easy#semi-supervised-learning)
- [Bayesian Networks](https://github.com/purvanshi/ML-Research-Made-Easy#bayesian-networks)



## NLP

| Title | Conf | supplementary material |
|:--------|:--------:|:--------:|
| [BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding](https://arxiv.org/abs/1810.04805) | ArXiv | [Google Blog](https://ai.googleblog.com/2018/11/open-sourcing-bert-state-of-art-pre.html), [Code](https://github.com/google-research/bert), [Medium](https://medium.com/syncedreview/best-nlp-model-ever-google-bert-sets-new-standards-in-11-language-tasks-4a2a189bc155) |


#### 1. Natural Language Inference 
Leader board: 

[Stanford Natural Language Inference (SNLI)](https://nlp.stanford.edu/projects/snli/)

[MultiNLI](https://www.kaggle.com/c/multinli-matched-open-evaluation/leaderboard)

LSTM [Colah](http://colah.github.io/posts/2015-08-Understanding-LSTMs/) , [Andrej karpathy](http://karpathy.github.io/2015/05/21/rnn-effectiveness/)

| Title | Conf | supplementary material |
|:--------|:--------:|:--------:|
| [Subword Semantic Hashing for Intent Classification on Small Datasets](https://arxiv.org/abs/1810.07150) | ArXiv | [Blog](https://chatbotslife.com/know-your-intent-sota-results-in-intent-classification-8e1ca47f364c) |



#### 2. Question Answering
Leader Board

[SQuAD](https://rajpurkar.github.io/SQuAD-explorer/)



## Computer Vision
| Title | Conf | supplementary material | 
|:--------|:--------:|:--------:|
| [Video-to-Video Synthesis](https://arxiv.org/abs/1808.06601) | NIPS | [video](https://youtu.be/5zlcXTCpQqM), [Blog](https://ww.techleer.com/articles/549-video-to-video-synthesis-at-2k-resolution-using-a-conditional-gan-framework/), [Reddit](https://www.reddit.com/r/MachineLearning/comments/98ulq8/videotovideo_synthesis_from_nvidia_with_code_r/)  |
| [SSD: Single Shot MultiBox Detector](https://arxiv.org/abs/1512.02325) | ECCV | [video](https://www.youtube.com/watch?v=nDPWywWRIRo), [Blog](https://towardsdatascience.com/understanding-ssd-multibox-real-time-object-detection-in-deep-learning-495ef744fab),[Pytorch](https://github.com/amdegroot/ssd.pytorch)|
| [Capsule Networks](https://arxiv.org/abs/1710.09829) | ArXiv | [Blog](https://cezannec.github.io/Capsule_Networks/), [Blog Series](https://medium.com/ai%C2%B3-theory-practice-business/understanding-hintons-capsule-networks-part-i-intuition-b4b559d1159b)|
| [GAN](https://arxiv.org/pdf/1406.2661) | ArXiv | [Blog](https://www.analyticsvidhya.com/blog/2017/06/introductory-generative-adversarial-networks-gans/), [Video](https://www.youtube.com/watch?v=HGYYEUSm-0Q),[Other gans](https://github.com/nightrome/really-awesome-gan),[Gan blog series](https://medium.com/@jonathan_hui/gan-gan-series-2d279f906e7b)|
| [deep clustering for unsupervised learning of visual features](https://arxiv.org/abs/1807.05520) | ECCV | [Blog](https://xiaoyuliu.github.io/2018/08/09/20180809-Deep-Cluster-md/), [Reddit](https://www.reddit.com/r/MachineLearning/comments/90k86x/r_deep_clustering_for_unsupervised_learning_of/)|
|[You Only Look Once: Unified, Real-Time Object Detection](https://arxiv.org/abs/1506.02640)| ArXiv | [Blog] (https://medium.com/@krishamehta/8-you-only-look-once-unified-real-time-object-detection-3a56e333b206), [Tensorflow tutorial youtube](https://www.youtube.com/watch?v=4eIBisqx9_g)|

## Speech
[Speech SOTA](https://github.com/syhw/wer_are_we)
<br>
[Music Genre classification](https://hackernoon.com/finding-the-genre-of-a-song-with-deep-learning-da8f59a61194)

## Semi Supervised Learning


## Supervised Learning
[Medical data](https://github.com/beamandrew/medical-data)<br>
[Batch Normalization](https://www.learnopencv.com/batch-normalization-in-deep-networks/)
## Transfer Learning



## Reinforcement Learning

## Bayesian Networks


| Title | Conf | supplementary material |
|:--------|:--------:|:--------:|
| [Bayesian Convolutional Neural Networks with Variational Inference](https://arxiv.org/abs/1806.05978) | ArXiv | [Blog](https://medium.com/neuralspace/bayesian-convolutional-neural-networks-with-bayes-by-backprop-c84dcaaf086e) |
| [Local Reparameterization Trick](https://arxiv.org/abs/1506.02557) | ArXiv | [Blog](https://medium.com/@llionj/the-reparameterization-trick-4ff30fe92954) |
| [Bayes by Backprop](https://arxiv.org/abs/1505.05424) | ArXiv | [Blog](https://medium.com/neuralspace/probabilistic-deep-learning-bayes-by-backprop-c4a3de0d9743) |
